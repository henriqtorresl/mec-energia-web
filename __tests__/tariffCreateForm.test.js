describe('Test functions from TariffCreateForm', () => {
  it('isValidEndDate', () => {
    const date1 = null;
    const date2 = new Date("2009-12-31");
    const date3 = new Date("2010-01-01");

    const startDate = new Date("2010-01-01");

    const isValid = jest.fn((date) => {
      if (date == null) {
        return false;
      }
      return true;
    });

    const isAfter = jest.fn((date, datePreDefined) => {
      if (date >= datePreDefined) {
        return true;
      }
    });

    const isValidEndDate = jest.fn((date) => {
      if (!date || !isValid(date)) {
        return "Insira uma data válida no formato dd/mm/aaaa.";
      }

      if (!isAfter(new Date(date), startDate)) {
        return "Insira uma data posterior à data de início";
      }

      return true;
    });

    expect(isValidEndDate(date1)).toBe("Insira uma data válida no formato dd/mm/aaaa.");
    expect(isValidEndDate(date2)).toBe("Insira uma data posterior à data de início");
    expect(isValidEndDate(date3)).toBe(true);
  });
});
